#include <iomanip>
#include <iostream>
#include <list>
#include <string>

class Utils {
public:
  static time_t parseDateFromStr(std::istringstream &raw_date,
                                 const std::string &date_format = "%m/%d/%Y") {
    std::tm t = {0};
    raw_date >> std::get_time(&t, date_format.c_str());
    return mktime(&t);
  }
  static bool strIcmp(std::string string_a, std::string string_b) {
    if (string_a.size() != string_b.size()) {
      return false;
    }
    return (std::equal(string_a.begin(), string_a.end(), string_b.begin(),
                       [](char &char_a, char &char_b) {
                         return std::tolower(char_a) == std::tolower(char_b);
                       }));
  }
};

template <class TCategory> class Category {
public:
  Category(void){};
  Category(const std::string &tmp_status,
           const TCategory &tmp_reference_limit) {
    this->setStatus(tmp_status);
    this->setReferenceLimit(tmp_reference_limit);
  }
  inline void setStatus(const std::string &new_status) {
    this->status = new_status;
  }
  inline void setReferenceLimit(const TCategory &new_condition) {
    this->reference_limit = new_condition;
  }
  inline std::string getStatus(void) const { return this->status; };
  inline TCategory getReferenceLimit(void) const {
    return this->reference_limit;
  };
  inline virtual bool Calculate(const TCategory &check_item) const {
    return check_item > this->reference_limit ? true : false;
  }

private:
  std::string status;
  TCategory reference_limit;
};

class TradeInfo {
public:
  TradeInfo(const std::string &raw_opinfo) { this->parseRawOInfo(raw_opinfo); }
  inline double getValue(void) const { return this->value; };
  inline std::string getClientSector(void) const {
    return this->client_sector;
  };
  inline auto getNextPaymentDate(void) const {
    return localtime(&this->next_payment_date);
  };
  inline time_t getEpochNextPaymentDate(void) const {
    return this->next_payment_date;
  };
  inline void setValue(const double &tmp_value) { this->value = tmp_value; };
  inline void setClientSector(const std::string &tmp_cs) {
    this->client_sector = tmp_cs;
  };
  inline void setNextPaymentDate(const time_t &tmp_date) {
    this->next_payment_date = tmp_date;
  };

private:
  double value;
  std::string client_sector;
  time_t next_payment_date;
  void parseRawOInfo(const std::string &raw_opinfo) {
    double tmp_value = 0;
    std::string tmp_sc;
    std::istringstream iss_ro(raw_opinfo);
    iss_ro >> tmp_value;
    iss_ro >> tmp_sc;
    this->setValue(tmp_value);
    this->setClientSector(tmp_sc);
    this->setNextPaymentDate(Utils::parseDateFromStr(iss_ro));
  }
};

class Trade : public TradeInfo {
public:
  Trade(const std::string &tmp_ref_date, const std::string &raw_trade_info)
      : TradeInfo(raw_trade_info) {
    std::istringstream ss_tmp_date(tmp_ref_date);
    this->ref_date = Utils::parseDateFromStr(ss_tmp_date);
  }
  void processTrade(void) {
    // possible previous funcs
    this->validator();
  }
  inline std::string getStatus(void) const { return this->status; };

private:
  time_t ref_date;
  std::string status = "Unprocessed";
  bool validator(void) {
    Category<time_t> defaulted("Defaulted",
                               this->ref_date + (2592000 /*30 days*/));
    Category<double> highrisk("Highrisk", 1000000);
    Category<double> mediumrisk("Mediumrisk", 1000000);
    this->status = "OK";
    if (!defaulted.Calculate(this->getEpochNextPaymentDate())) {
      this->status = defaulted.getStatus();
      return true;
    }
    if (Utils::strIcmp(this->getClientSector(), "private")) {
      if (highrisk.Calculate(this->getValue())) {
        this->status = highrisk.getStatus();
        return true;
      }
    }
    if (Utils::strIcmp(this->getClientSector(), "public")) {
      if (mediumrisk.Calculate(this->getValue())) {
        this->status = mediumrisk.getStatus();
        return true;
      }
    }
    return false;
  }
};

int main(void) {
  std::string ref_date = "";
  std::string raw_trade = "";
  std::list<std::string> lst_rt;
  int number_of_trades = 0;
  std::cin >> ref_date;
  std::cin >> number_of_trades;
  std::cin.ignore();
  for (int i = 0; i < number_of_trades; i++) {
    std::getline(std::cin, raw_trade, '\n');
    lst_rt.push_back(raw_trade);
  }
  std::cout << "\n\n";
  for (auto it_rt = lst_rt.begin(); it_rt != lst_rt.end(); it_rt++) {
    Trade trade(ref_date, *it_rt);
    trade.processTrade();
    std::cout << trade.getStatus() << std::endl;
  }
  return 0;
}